package poste;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Courriel extends ObjetPostal{
	
	private String adresseElecDest;
	private String titre;
	private String corps;
	private List<String> listPiecesJointes = new ArrayList<String>();
	//adresse électronique de destination, un titre de message, un corps de message et des pièces jointes
	
	private static float tarifBase=0;
	private static String typeObjetPostal="Courriel";
	
	public Courriel()
	{// implicitement : super();
		adresseElecDest="test@test";
		titre="vide";
		corps="vide";
	}



	public Courriel(String origine, String destination, String codePostal, float poids, float volume,
			Recommandation tauxRecommandation, String adresseElecDest, String titre,String corps) {
		super(origine, destination, codePostal, poids, volume, tauxRecommandation); // appel au constructeur de la super classe
		this.adresseElecDest = adresseElecDest;
		this.titre = titre;
		this.corps = corps;
	}

	@Override
	public float getTarifBase() {
		return tarifBase;
	}

	@Override
	public float tarifRemboursement() {
		return 0;
	}

	@Override
	public String typeObjetPostal() {
		return typeObjetPostal;
	}
	
	public boolean envoyer() {
		boolean valideEmail = Pattern.matches(".+@.+", adresseElecDest);
		return valideEmail;
	}

}
