package poste;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void testLettre()
    {
    	Lettre lettre = new Lettre();
    	assertNotNull(lettre.isUrgence());
    	float tarifAffranchissement = (float)0.5;
    	float tarifRemboursement = 0;
    	if(lettre.getTauxRecommandation()==Recommandation.zero) {
    		tarifRemboursement=0f;
    	}
    	if(lettre.getTauxRecommandation()==Recommandation.un) {
    		tarifAffranchissement+=0.5f;
    		tarifRemboursement=1.5f;
    	}
    	if(lettre.getTauxRecommandation()==Recommandation.deux) {
    		tarifAffranchissement+=1.5f;
    		tarifRemboursement=15f;
    	}
    	if(lettre.isUrgence()) {
    		tarifAffranchissement+=0.3f;
    	}
    	assertEquals(lettre.tarifAffranchissement(),tarifAffranchissement,0.0f);
    	assertEquals(lettre.tarifRemboursement(),tarifRemboursement,0.0f);
    	
    	String decrit = "Lettre "+lettre.getCodePostal()+"/"+lettre.getDestination()+"/"+lettre.getTauxRecommandation();
    	if (lettre.isUrgence()) decrit += "/urgence";
    	else decrit += "/ordinaire";
    	assertEquals(decrit,lettre.toString());
    	
    }
    @Test
    public void testColis()
    {
    	Colis colis = new Colis();
    	assertNotNull(colis.getDeclareContenu());
    	assertNotNull(colis.getValeurDeclaree());
    	float tarifAffranchissement = 2;
    	float tarifRemboursement = 0;
    	if(colis.getTauxRecommandation()==Recommandation.zero) {
    		tarifRemboursement=0f;
    	}
    	if(colis.getTauxRecommandation()==Recommandation.un) {
    		tarifAffranchissement+=0.5f;
    		tarifRemboursement=0.1f*colis.getValeurDeclaree();
    	}
    	if(colis.getTauxRecommandation()==Recommandation.deux) {
    		tarifAffranchissement+=1.5f;
    		tarifRemboursement=0.5f*colis.getValeurDeclaree();
    	}
    	
    	assertEquals(colis.tarifAffranchissement(),tarifAffranchissement,0.0f);
    	assertEquals(colis.tarifRemboursement(),tarifRemboursement,0.0f);
    	
    	String decrit = "Colis "+colis.getCodePostal()+"/"+colis.getDestination()+"/"+colis.getTauxRecommandation()+"/"+colis.getVolume()+"/"+colis.getValeurDeclaree();
    	assertEquals(decrit,colis.toString());
    }
    @Test
    public void testCourriel()
    {
    	Courriel courriel = new Courriel();
    	boolean test = courriel.envoyer();
    	System.out.println(test);
    }
}
